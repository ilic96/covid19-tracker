package com.example.retroroom.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.example.retroroom.service.local.AppDatabase
import com.example.retroroom.service.model.CountryItem
import com.example.retroroom.service.network.ServiceApi
import com.example.retroroom.service.repository.CountryRepo

class CountryViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CountryRepo
    var list: List<CountryItem>? = null

    init {
        val countryDao = AppDatabase.getDatabase(application).countryDao()
        val serviceApi = ServiceApi.buildService(ServiceApi::class.java)
        repository = CountryRepo(countryDao, serviceApi)
    }

    suspend fun fetchAllCountries() {
        list = repository.fetchAllCountries()
        repository.clear()
        for (x in list!!) {
            repository.insertSingleCountry(x)
            Log.d("test", x.toString())
        }
        /*list?.forEach {
            repository.insertSingle(it)
        }*/
    }
    suspend fun loadAllCountries() {
      list = repository.loadAllCountries()
    }
}