package com.example.retroroom.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.retroroom.R
import com.example.retroroom.service.model.CountryItem
import kotlinx.android.synthetic.main.recyclerview_item.view.*

class Adapter(context: Context, private val countries: List<CountryItem>) : RecyclerView.Adapter<Adapter.MyViewHolder>(){
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    //private var countries = listOf<CountryItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.MyViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int = countries.size

    override fun onBindViewHolder(holder: Adapter.MyViewHolder, position: Int) {
        val current = countries[position]
        holder.name.text = current.Country
        holder.code.text = current.ISO2
    }

  /*  fun setData(countries: List<CountryItem>) {
        this.countries = countries
        notifyDataSetChanged()
    } */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.countryName
        val code: TextView = itemView.countryCode
    }
}