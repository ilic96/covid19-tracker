package com.example.retroroom.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retroroom.R
import com.example.retroroom.service.local.AppDatabase
import com.example.retroroom.service.model.CountryItem
import com.example.retroroom.utils.ConnectionUtil
import com.example.retroroom.viewmodel.CountryViewModel
import com.wajahatkarim3.roomexplorer.RoomExplorer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: CountryViewModel
    var test: List<CountryItem>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                CountryViewModel::class.java
            )

        lifecycleScope.launch {
            if (ConnectionUtil.isOnline(this@MainActivity)) {
                viewModel.fetchAllCountries()
                test = viewModel.list
            }

            viewModel.loadAllCountries()
            test = viewModel.list
            val adapter = test?.let { Adapter(this@MainActivity, it) }
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        RoomExplorer.show(this, AppDatabase::class.java, "covid19_db")
        return super.onKeyDown(keyCode, event)
    }
}

