package com.example.retroroom.service.local

import androidx.room.*
import com.example.retroroom.service.model.CountryItem
import java.nio.charset.CodingErrorAction.REPLACE

@Dao
interface CountryDao {

    @Query("SELECT * FROM CountryItem")
    suspend fun getAllCountries(): List<CountryItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInLocal(newData: List<CountryItem>)

    @Insert
    suspend fun insertCountryItem(countryItem: CountryItem)

    @Query("DELETE FROM CountryItem")
    suspend fun deleteAll()
}