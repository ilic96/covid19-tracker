package com.example.retroroom.service.repository

import com.example.retroroom.service.local.CountryDao
import com.example.retroroom.service.model.CountryItem
import com.example.retroroom.service.network.ServiceApi

class CountryRepo(private val countryDao: CountryDao, private val serviceApi: ServiceApi) {

   suspend fun fetchAllCountries(): List<CountryItem> {
      return serviceApi.fetchAllCountries()
   }
    suspend fun insertAll(newData: List<CountryItem>) {
       countryDao.insertInLocal(newData)
    }
    suspend fun insertSingleCountry(item: CountryItem) {
        countryDao.insertCountryItem(item)
    }
    suspend fun clear() {
        countryDao.deleteAll()
    }
    suspend fun loadAllCountries(): List<CountryItem> {
        return countryDao.getAllCountries()
    }
}