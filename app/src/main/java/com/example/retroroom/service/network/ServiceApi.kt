package com.example.retroroom.service.network

import com.example.retroroom.service.model.CountryItem
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ServiceApi {

    @GET("countries")
    suspend fun fetchAllCountries(): List<CountryItem>

    companion object {
        private val client = OkHttpClient.Builder().build()
        private const val BASE_URL= "https://api.covid19api.com/"

        private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun<T> buildService(service: Class<T>): T {
            return retrofit.create(service)
        }
    }
}