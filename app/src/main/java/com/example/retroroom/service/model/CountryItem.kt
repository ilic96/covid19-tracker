package com.example.retroroom.service.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class CountryItem(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val Country: String = "",
    val ISO2: String = ""
)