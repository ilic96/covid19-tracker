package com.example.retroroom.service.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class CountryList(
    val id: Int = -1
) : ArrayList<CountryItem>()
